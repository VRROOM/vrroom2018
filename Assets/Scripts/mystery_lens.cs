﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mystery_lens : MonoBehaviour {

	public GameObject camera;
	public GameObject lens;
	public GameObject colorCombination;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay (Collider other){
	
		if (other.transform.gameObject == lens) {

			colorCombination.SetActive (true);
		}
			
	
	}

	void OnTriggerExit (Collider other){
	
	
		if (other.transform.gameObject == lens) {
			colorCombination.SetActive (false);
		
		}
	}
}
