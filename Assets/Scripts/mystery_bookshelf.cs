﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mystery_bookshelf: MonoBehaviour {

	public Transform bookshelf;

	public int pullAngle;
	public float maxAngle;

	private Vector3 torchAngles;


	// Use this for initialization
	void Start () {

		torchAngles = transform.eulerAngles;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (transform.eulerAngles.z >= pullAngle) {
			mysteryOne ();
		}

		if (transform.eulerAngles.z >= maxAngle) {
			torchAngles.z = maxAngle;
			transform.eulerAngles = torchAngles;
		}
		
	}

	void mysteryOne() {
		
		if (bookshelf.position.z >= 9) {

			bookshelf.Translate (Vector3.forward * Time.deltaTime);
		}


	}
}
