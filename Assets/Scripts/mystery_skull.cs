﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mystery_skull: MonoBehaviour {

	public GameObject bone;
	public GameObject skull;
	public GameObject key;


	private Vector3 bonePosition;
	private int skullCounter;



	// Use this for initialization
	void Start () {

		bonePosition = bone.transform.position;
		skullCounter = 0;
		
	}
	
	// Update is called once per frame
	void Update () {


		
	}

	void OnTriggerEnter (Collider other) {


		if (other.GetComponent<Skull>())
		{
			skullCounter = skullCounter+1;
			Debug.Log (skullCounter);

			if (skullCounter == 6) {

				key.SetActive (true);
			}
		}

		if (other.transform.gameObject == bone) {

			bone.transform.position = bonePosition;
		}


	
	}
}
