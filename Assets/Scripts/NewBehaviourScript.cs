﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

	public Material[] material;
	Renderer rend;

	void Start ()
	{
		rend = GetComponent<Renderer>();
		rend.enabled = true;
		rend.sharedMaterial = material [0];
	}

	void Update ()
	{
		if ((GameObject.FindGameObjectWithTag("Blau").GetComponent<Zutat1> ().imTopf) & (GameObject.FindGameObjectWithTag("Rot").GetComponent<Zutat2> ().imTopf)) 
		{
			rend.sharedMaterial = material [2];
			GameObject.FindGameObjectWithTag ("Blau").GetComponent<Zutat1> ().Respawn ();
			GameObject.FindGameObjectWithTag ("Rot").GetComponent<Zutat2> ().Respawn ();


		}

		if ((GameObject.FindGameObjectWithTag("Blau").GetComponent<Zutat1> ().imTopf) & (GameObject.FindGameObjectWithTag("Gelb").GetComponent<Zutat3> ().imTopf)) 
		{
			rend.sharedMaterial = material [3];
			GameObject.FindGameObjectWithTag ("Gelb").GetComponent<Zutat3> ().Respawn ();
			GameObject.FindGameObjectWithTag ("Blau").GetComponent<Zutat1> ().Respawn ();

		} 

		if ((GameObject.FindGameObjectWithTag("Gelb").GetComponent<Zutat3> ().imTopf) & (GameObject.FindGameObjectWithTag("Rot").GetComponent<Zutat2> ().imTopf)) 
		{
			rend.sharedMaterial = material [1];
			GameObject.FindGameObjectWithTag ("Gelb").GetComponent<Zutat3> ().Respawn ();
			GameObject.FindGameObjectWithTag ("Rot").GetComponent<Zutat2> ().Respawn ();

		} 
			

	}
}
